Apakah perbedaan antara JSON dan XML?
Perbedaan antara JSON dan XML

Definisi JSON dan XML
XML adalah versi sederhana dari SGML yang digunakan untuk menyimpan dan mewakili data terstruktur dalam format yang bisa dibaca oleh mesin dan 
bisa dibaca oleh manusia. Ini dirancang untuk meningkatkan keterbacaan karena merupakan bahasa markup yang menambahkan informasi tambahan ke teks biasa. JSON, 
di sisi lain, adalah format pertukaran-data yang ringan yang digunakan untuk merepresentasikan data hierarkis dan didasarkan pada sintaksis objek JavaScript..

Arti JSON dan XML
XML adalah kependekan dari "Extensive Markup Language" dan merupakan teknologi berorientasi dokumen yang digunakan untuk menyandikan data dalam format yang dapat dibaca manusia. 
Ini adalah format file fleksibel yang cocok untuk penggunaan web. JSON adalah singkatan dari "Notasi Objek JavaScript" dan seperti namanya, ini didasarkan pada bahasa pemrograman JavaScript.

Tujuan JSON dan XML
XML dikembangkan oleh World Wide Web Consortium sebagai format standar terbuka yang terdokumentasi dengan baik yang berisi seperangkat aturan tentang cara menyandikan dokumen 
dalam format yang dapat dibaca manusia dan yang dapat dibaca mesin. JSON dikembangkan oleh Douglas Crockford sebagai format file yang sederhana dan ringan untuk pertukaran data.

Sintaks JSON dan XML
JSON tidak memiliki tag awal dan akhir dan sintaksisnya lebih ringan daripada XML karena berorientasi pada data dengan redundansi lebih sedikit yang menjadikannya alternatif
yang ideal untuk menukar data melalui XML. XML, di sisi lain, membutuhkan lebih banyak karakter untuk mewakili data yang sama. Ini tidak seringan JSON.

Jenis data dalam JSON dan XML
JSON mendukung tipe data teks dan angka termasuk bilangan bulat dan string. Data terstruktur direpresentasikan menggunakan array dan objek. XML tidak memiliki dukungan langsung 
untuk tipe array tetapi mendukung banyak tipe data seperti angka, teks, gambar, grafik, grafik, dll.

Apakah perbedaan antara HTML dan XML?
XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
XML didorong konten sedangkan HTML didorong oleh format.
XML itu Case Sensitive (peka terhadap huruf besar dan kecil) sedangkan XML Case Insensitive (tidak peka huruf besar dan kecil)
XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
XML strict untuk tag penutup dimana wajib untuk menutup setiap tag yangi digunakan sedangkan HTML tidak strict.
Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
Tag XML tidak ditentukan sebelumnya sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.

Referensi :
https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html 

