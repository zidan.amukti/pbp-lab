import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab_7/widget/button_widget.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'TextFormField';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primaryColor: Colors.red),
        home: MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final formKey = GlobalKey<FormState>();
  String makan = '';
  String minum = '';
  String tanggal = '';
  String waktu = '';

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.list),
          title: Image.asset("assets/logo.png"),
          backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        ),
        body: Form(
          key: formKey,
          //autovalidateMode: AutovalidateMode.onUserInteraction,
          child: ListView(
            padding: EdgeInsets.all(16),
            children: [
              buildTanggal(),
              const SizedBox(height: 16),
              buildWaktu(),
              const SizedBox(height: 16),
              buildmakan(),
              const SizedBox(height: 16),
              buildminum(),
              const SizedBox(height: 16),
              buildSubmit(),
            ],
          ),
        ),
      );

  Widget buildmakan() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Makan',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Makan tidak boleh kosong';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => makan = value!),
      );

  Widget buildminum() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Minum',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Masa Ngak Minum';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => minum = value!),
      );

  Widget buildTanggal() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Tanggal',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Tanggal tidak boleh kosong';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => tanggal = value!),
      );

  Widget buildWaktu() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Waktu',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Waktu tidak boleh kosong';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => waktu = value!),
      );

  Widget buildSubmit() => Builder(
        builder: (context) => ButtonWidget(
          text: 'Submit',
          onClicked: () {
            final isValid = formKey.currentState!.validate();
            // FocusScope.of(context).unfocus();

            if (isValid) {
              formKey.currentState!.save();

              final message =
                  'Makan: $makan\nMinum: $minum\nTanggal: $tanggal\nWaktu: $waktu';
              final snackBar = SnackBar(
                content: Text(
                  message,
                  style: TextStyle(fontSize: 20),
                ),
                backgroundColor: Colors.green,
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          },
        ),
      );
}
