from django.http import response
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForms
from django.http.response import HttpResponseRedirect


# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note': note}
    print(response)
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForms()
    if request.method == 'POST':
        form = NoteForms(request.POST)
        form.save()
        return HttpResponseRedirect('/lab-4/')

    context = {'form' : form}
    return render(request, 'lab4_form.html', context)

def note_list(request):
    note = Note.objects.all()
    response = {'note': note}
    print(response)
    return render(request, 'lab4_note_list.html', response)