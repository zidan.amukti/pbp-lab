from django.db import models
from django.db.models import fields

from lab_2.models import Note
from django.forms.models import ModelForm

class NoteForms(ModelForm):
    class Meta:
        model = Note
        fields = "__all__" 