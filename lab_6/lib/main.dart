import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: "Poppins",
        canvasColor: Color.fromRGBO(232, 247, 252, 1),
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.list),
          title: Image.asset("assets/logo.png"),
          backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        ),
        body: Container(
          child: ListView(
            children: [
              Form(
                key: _formKey,
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  child: Column(children: [
                    SizedBox(
                      height: 20,
                    ),
                    Image.asset("assets/Food-Schedule.png"),

                    SizedBox(
                      height: 20,
                    ),
                    // TextField(),
                    TextFormField(
                      decoration: new InputDecoration(
                        labelText: "Tanggal",
                        icon: Icon(Icons.calendar_today),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Tanggal tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      decoration: new InputDecoration(
                        labelText: "Waktu Makan",
                        icon: Icon(Icons.timelapse_outlined),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Waktu Makan tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    TextFormField(
                      decoration: new InputDecoration(
                        labelText: "Makan",
                        icon: Icon(Icons.food_bank),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Makan tidak boleh kosong';
                        }
                        return null;
                      },
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    TextFormField(
                      decoration: new InputDecoration(
                        labelText: "Minum",
                        icon: Icon(Icons.coffee),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Minum Woi';
                        }
                        return null;
                      },
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    RaisedButton(
                      child: Text(
                        "Tambahkan",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          print("Makan Dan Minum Berhasil Ditambahkan");
                        }
                      },
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    RaisedButton(
                      child: Text(
                        "Ringkasan",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          print("Liat Ringkasan Kak");
                        }
                      },
                    ),
                  ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
