from django.http import response
from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForms
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect


# Create your views here.
@login_required(login_url = '/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    print(response)
    return render(request, 'lab3_index.html', response)

@login_required(login_url = '/admin/login/')
def add_friend(request):
    form = FriendForms()
    if request.method == 'POST':
        form = FriendForms(request.POST)
        form.save()
        return HttpResponseRedirect('/lab-3/')

    context = {'form' : form}
    return render(request, 'lab3_form.html', context)