from django.db import models
from django.db.models import fields

from lab_1.models import Friend
from django.forms.models import ModelForm

class FriendForms(ModelForm):
    class Meta:
        model = Friend
        fields = "__all__" 