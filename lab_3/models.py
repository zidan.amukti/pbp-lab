from django.db import models

# Create your models here.

class Friend(models.Model):
    Name = models.CharField(max_length=30)
    NPM = models.IntegerField()
    DOB = models.DateField()